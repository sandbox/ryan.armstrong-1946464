CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation


INTRODUCTION
------------

This module provides a plugin for Views 3.x that allows you to grab a 
variable in any one of the superglobals and use them as a default value 
for a contextual filter. The most commonly used superglobal would be 
$_GET, which would allow you to pull any variables that have been passed 
via the URL into the contextual filter.

Example Use Cases

- The Search API and Facet API modules both pass their filter values via 
the URL. This module would allow you grab those values via $_GET and use 
them in a secondary view, such as a 'Suggested Results' block or some 
sort of related content.

It currently supports all of the superglobals and can traverse any 
number of nested arrays or objects. If that particular variable is not 
found within your selected superglobal, it will output an Drupal error 
message letting you know.

INSTALLATION
------------

1. Enable the module and the superglobal options will appear under the
   default value section of a contextual filter.
