<?php

/**
 * @file
 * Contains the raw value argument default plugin.
 */

/**
 * Default argument plugin to use the raw value from the URL.
 *
 * @ingroup views_argument_default_plugins
 */
class views_plugin_argument_default_superglobals extends views_plugin_argument_default {
  function option_definition() {
    $options = parent::option_definition();
    $options['superglobal'] = array('default' => '$_GET');
    $options['superglobal_variable'] = array('default' => '');

    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    // Set $options to the supported superglobals.
    $superglobal_options = array(
      '$_SERVER' => '$_SERVER',
      '$_GET' => '$_GET',
      '$_POST' => '$_POST',
      '$_FILES' => '$_FILES',
      '$_COOKIE' => '$_COOKIE',
      '$_SESSION' => '$_SESSION',
      '$_REQUEST' => '$_REQUEST',
      '$_ENV' => '$_ENV',
    );

    $form['superglobal'] = array(
      '#type' => 'select',
      '#title' => t('Superglobal'),
      '#default_value' => $this->options['superglobal'],
      '#options' => $superglobal_options,
      '#description' => t('Choose the supervariable you would like to access.'),
    );
    $form['superglobal_variable'] = array(
      '#type' => 'textfield',
      '#title' => t('Variable to access'),
      '#description' => t("Choose which variable you want to access. For example, if you wanted to use $_GET ['variable']['subvariable'], you would enter <em>variable subvariable</em>. Seperate nested variables with a space. Can traverse arrays and objects."),
      '#default_value' => $this->options['superglobal_variable'],
    );
  }

  function get_argument() {
    // Initially set $arg to the top-level of the superglobal. Variable
    // variables do not work with superglobals, so a control structure is
    // required. Would like to replace this with something more elegant.
    switch ($this->options['superglobal']) {
      case '$_SERVER':
        $arg = $_SERVER;
        break;

      case '$_GET':
        $arg = $_GET;
        break;

      case '$_POST':
        $arg = $_POST;
        break;

      case '$_FILES':
        $arg = $_FILES;
        break;

      case '$_COOKIE':
        $arg = $_COOKIE;
        break;

      case '$_SESSION':
        $arg = $_SESSION;
        break;

      case '$_REQUEST':
        $arg = $_REQUEST;
        break;

      case '$_ENV':
        $arg = $_ENV;
        break;
    }

    // Traverse the superglobal with the path given and set $arg to the value.
    // Display an error message if the variable isn't found.
    foreach (explode(' ', $this->options['superglobal_variable']) as $key) {
      if (is_array($arg) && array_key_exists($key, $arg)) {
        $arg = $arg[$key];
      }
      elseif (is_object($arg) && property_exists($arg, $key)) {
        $arg = $arg->$key;
      }
    }

    return filter_xss($arg);
  }
}
